==================================================================
https://keybase.io/calinou
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://hugo.pro
  * I am calinou (https://keybase.io/calinou) on keybase.
  * I have a public key with fingerprint 9F85 9CF4 33E5 1DDD C9FA  2F4F 46AC E49F 6168 5096

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101e7bb1187c42d4dba9b512389fdc7b3f8b31c714571a8026aea644dfeecdbcd9c0a",
      "fingerprint": "9f859cf433e51dddc9fa2f4f46ace49f61685096",
      "host": "keybase.io",
      "key_id": "46ace49f61685096",
      "kid": "0101e7bb1187c42d4dba9b512389fdc7b3f8b31c714571a8026aea644dfeecdbcd9c0a",
      "uid": "2c540bd6615297abaf13f0ca38f6ae19",
      "username": "calinou"
    },
    "service": {
      "hostname": "hugo.pro",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1508098093,
  "expire_in": 157680000,
  "prev": "ae5f9b06543a4a6c4a1e0b14b121f9a3e78886981123f8743ecdf40b5b422ea3",
  "seqno": 15,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.76
Comment: https://keybase.io/crypto

yMIeAnicrVJZSFRRGB63Qtszs4VebphBk91z9zsRGUHQ8mCKDUE2nHW8aHOnO4uJ
SFSUCCXBgBmIFRVtYkZapJQmLWIEUaS0gaSkpVAvZWhR50q99djhcA7n5/u+838f
/4M5KZ6MJHP0+z2x9UJj0pP7iZhn91D32ioB2aRS8FUJZXT6ouWERqKBMosIPkEE
IqA6QgAYOlYkohAETaQCSTZMRrCOZGYgGWAdKKoOoCFKGqRQUxTCKMUEYWJiEQpe
gVmhIHXCjhWKclmTGaqJmSLLVAWEEGwyKDGFKRrEVDGZBjRDFU2NE0vtiMvgzSEY
oXmWzWv8EZhu7x/4/9x3bFpOwqoiIqJpQJVMHSLIgMxEDGWDcR4wXWCEOiG4j3I0
huVWyI4J1V6BF+MWpm6urpE/gNJY0M4LO64VfkZtbJe71Wg0HPG5rGhl2IVVUBT4
IxBAVojwCDkjTp2IZYcEH+BIHLVcRaCKhmjyLXsFeiBsOTRguQhV1wyRL/cfGueS
kKrMRKKmKjJUoIYVCKiIgIKABJgJZaobhqGZBuBJMUNXZB4G49ZVpEgShbLgWtof
sl1t3icMcs2IFQzBaMyhQnVP955UT1KGZ0Zasjtbnoz0+X8n7kznzF8lb3Mm/Vuc
HdsW5X27Hc9d/2jZbPFafuGuvUQvOD30tF9MuZV9/kt99wFnEOuJRrjtaHrL277C
5syHEzuXTgVTxuW0lsMDSwqaFhfn55L4wdVHFvTUJtPMeU3n+sbKLt0YHOgfHD30
8OuKuoXL76MLzVm5p65fSZ2lZ/kSfQ2XE5e3Qr+/dtGviseJluGj1dV3XmfrH42R
qzdHamKbxzbWt/6caE/qGi4d/FyU0z1R27KOXuz9dCz9w3BvScZIAfCvilQeq1pd
uGGo6t2m7SvzO6ZOSG1db0YWeNsTP5qKJ4+fBN629z01pXfRvdbOutvpa16Mv2w4
S149K+qoLJnb1fj8N0dbSUY=
=ApIz
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/calinou

==================================================================

