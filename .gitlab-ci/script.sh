#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

# Build the website
# Install Ruby dependencies in `vendor/` so that they can be cached by GitLab CI
bundle install --deployment --path vendor "-j$(nproc)"
yarn install --production --frozen-lockfile
yarn run prod

# Minify files (including HTML)
curl -LO "https://github.com/tdewolff/minify/releases/download/v2.7.4/minify_2.7.4_linux_amd64.tar.gz"
tar xf minify*.tar.gz
./minify -r dist -o dist
