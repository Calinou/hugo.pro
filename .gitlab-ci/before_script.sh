#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

# Speed up installation in CI
export NOKOGIRI_USE_SYSTEM_LIBRARIES=true

# Install dependencies and non-snap Chromium
apt-get update -qq
apt-get install -qqq software-properties-common
add-apt-repository ppa:saiarcot895/chromium-beta
apt-get update -qq
apt-get install -qqq build-essential nodejs ruby ruby-dev curl openssh-client chromium-browser patch liblzma-dev zlib1g-dev
curl -fsS "https://dl.yarnpkg.com/debian/pubkey.gpg" | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee "/etc/apt/sources.list.d/yarn.list"
apt-get update -qq
apt-get install -qqq yarn
gem update --system
gem install bundler
