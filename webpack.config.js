const path = require('path')
const glob = require('glob')
const webpack = require('webpack')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')

const production = process.env.NODE_ENV === 'production'
const fileBasename = production ? '[name].[contenthash]' : '[name]'

module.exports = {
  mode: production ? 'production' : 'development',
  devtool: production ? false : 'cheap-module-eval-source-map',

  entry: {
    main: './src/static/js/main.js'
  },

  output: {
    filename: `${fileBasename}.js`,
    path: path.resolve(__dirname, 'dist/static')
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
            publicPath: ''
          }
        }]
      },
      {
        test: /\.(jpe?g|png|gif|webp)$/,
        loader: 'file-loader',
        options: {
          name: '[name.ext]',
          outputPath: 'images/'
        }
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: `${fileBasename}.css`
    }),
    new webpack.HashedModuleIdsPlugin()
  ],

  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      // Always split the vendor bundle even if it's small.
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all'
        }
      }
    },
    minimizer: [
      new TerserPlugin({ parallel: true }),
      new OptimizeCssAssetsPlugin()
    ]
  },

  watchOptions: {
    aggregateTimeout: 0
  }
}

if (production) {
  module.exports.plugins.push(
    // Generate a manifest that will be read by Jekyll
    new ManifestPlugin({
      basePath: '/static/',
      publicPath: '/static/',
      fileName: path.join(__dirname, 'src/_data/manifest.json')
    }),

    // Remove unused CSS
    new PurgecssPlugin({
      paths: glob.sync('src/**/*.{html,js}')
    })
  )
}
